const {
  TestUserGame,
  TestUserGameBiodata,
  TestUserGameHistory,
} = require("../../DB/models");
const bcrypt = require("bcrypt");

class UserController {
  static userRender(req, res) {
    TestUserGame.findAll({}).then((allUserList) => {
      console.log("ini all list =================================");
      for (let i = 0; i < allUserList.length; i++) {
        console.log(`username : ${allUserList[i].username}`);
      }
      res.send("complete");
    });
  }
  static async register(req, res) {
    const salt = await bcrypt.genSalt();
    const hashedPassword = await bcrypt.hash(req.body.password, salt);
    TestUserGame.findOne({ where: { email: req.body.email } }).then((user) => {
      if (user == null) {
        TestUserGame.create({
          username: req.body.username,
          password: hashedPassword,
          email: req.body.email,
        }).then((newuser) => {
          console.log("INI NEW USER========================");
          console.log(newuser);
          TestUserGameBiodata.create({
            date_birth: new Date(req.body.date_birth),
            city: req.body.city,
            TestUserGameId: newuser.id,
          }).then((biodata) => {
            console.log("INI NEW BIODATA===================");
            console.log(biodata);
            res.status("200").json({ message: "Oke" });
          });
        });
      } else {
        res.send("Username Already Exist");
      }
    });
  }
}

module.exports = UserController;
