var express = require("express");
var router = express.Router();
const UserController = require("../controllers/userscontroller");

// GET users listing ========================
router.get("/", UserController.userRender);

// POST register ============================
router.post("/register", UserController.register);
// POST Login ===============================
router.post("/login", (req, res) => {
  // later will be hashed
});

module.exports = router;
