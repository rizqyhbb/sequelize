"use strict";
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable("TestUserGameBiodata", {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      date_birth: {
        type: Sequelize.DATE,
      },
      city: {
        type: Sequelize.STRING,
      },
      TestUserGameId: {
        type: Sequelize.INTEGER,
        references: { model: "TestUserGames", key: "id" },
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable("TestUserGameBiodata");
  },
};
