"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class TestUserGame extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      TestUserGame.hasOne(models.TestUserGameBiodata);
      TestUserGame.hasMany(models.TestUserGameHistory);
    }
  }
  TestUserGame.init(
    {
      username: DataTypes.STRING,
      password: {
        type: DataTypes.STRING,
        validate: {
          notEmpty: {
            args: true,
            msg: "password is required",
          },
        },
      },
      email: {
        unique: true,
        type: DataTypes.STRING,
        validate: {
          isEmail: {
            args: true,
            msg: "invalid email format",
          },
        },
      },
    },
    {
      sequelize,
      modelName: "TestUserGame",
    }
  );
  return TestUserGame;
};
