"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class TestUserGameBiodata extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      TestUserGameBiodata.belongsTo(models.TestUserGame);
    }
  }
  TestUserGameBiodata.init(
    {
      date_birth: DataTypes.DATE,
      city: DataTypes.STRING,
      TestUserGameId: DataTypes.INTEGER,
    },
    {
      sequelize,
      modelName: "TestUserGameBiodata",
    }
  );
  return TestUserGameBiodata;
};
