"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class TestUserGameHistory extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      TestUserGameHistory.belongsTo(models.TestUserGame);
    }
  }
  TestUserGameHistory.init(
    {
      score: DataTypes.INTEGER,
      TestUserGameId: DataTypes.INTEGER,
    },
    {
      sequelize,
      modelName: "TestUserGameHistory",
    }
  );
  return TestUserGameHistory;
};
